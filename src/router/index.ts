import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/blog',
      name: 'posts',
      component: () => import('../views/BlogView.vue')
    },
    {
      path: '/post/:slug',
      name: 'post',
      component: () => import('../views/EntradaView.vue')
    },
    {
      path: '/contactanos',
      name: 'contactanos',
      component: () => import('../views/ContactoView.vue')
    },
    {
      path: '/libro-reclamaciones',
      name: 'reclamaciones',
      component: () => import('../views/ReclamacionesView.vue')
    },
    {
      path: '/registro',
      name: 'registro',
      component: () => import('../views/RegisterView.vue')
    },
    {
      path: '/confirmar-correo/:uuid',
      name: 'confirmar-correo',
      component: () => import('../views/RegisterConfirmView.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue')
    },
    {
      path: '/reset-account',
      name: 'reset-account',
      component: () => import('../views/ResetAccountView.vue')
    },
    {
      path: '/pruebas',
      name: 'pruebas',
      component: () => import('../views/PruebasView.vue')
    },
    {
      path: '/registro/adicionales/:uuid',
      name: 'registros-adicionales',
      component: () => import('../views/MyPerfil1View.vue')
    },
    {
      path: '/mi-perfil-2',
      name: 'mi-perfil-2',
      component: () => import('../views/MyPerfil2View.vue')
    },
    {
      path: '/mi-perfil-3',
      name: 'mi-perfil-3',
      component: () => import('../views/MyPerfil3View.vue')
    }
  ]
})

export default router
