import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import VueSocialSharing from 'vue-social-sharing'
import VueClipboard from 'vue-clipboard2'




const app = createApp(App)

app.use(router)
app.use(VueSocialSharing)
app.use(VueClipboard)


app.mount('#app')